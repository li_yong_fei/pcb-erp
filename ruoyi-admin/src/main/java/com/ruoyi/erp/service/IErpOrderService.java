package com.ruoyi.erp.service;

import java.util.List;
import com.ruoyi.erp.domain.ErpOrder;

/**
 * 订单Service接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface IErpOrderService 
{
    /**
     * 查询订单
     * 
     * @param id 订单ID
     * @return 订单
     */
    public ErpOrder selectErpOrderById(String id);

    /**
     * 查询订单列表
     * 
     * @param erpOrder 订单
     * @return 订单集合
     */
    public List<ErpOrder> selectErpOrderList(ErpOrder erpOrder);

    /**
     * 新增订单
     * 
     * @param erpOrder 订单
     * @return 结果
     */
    public int insertErpOrder(ErpOrder erpOrder);

    /**
     * 修改订单
     * 
     * @param erpOrder 订单
     * @return 结果
     */
    public int updateErpOrder(ErpOrder erpOrder);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpOrderByIds(String ids);

    /**
     * 删除订单信息
     * 
     * @param id 订单ID
     * @return 结果
     */
    public int deleteErpOrderById(String id);
}
