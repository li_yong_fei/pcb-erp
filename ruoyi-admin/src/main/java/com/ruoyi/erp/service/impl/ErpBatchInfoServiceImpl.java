package com.ruoyi.erp.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import org.apache.poi.ss.usermodel.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpBatchInfoMapper;
import com.ruoyi.erp.domain.ErpBatchInfo;
import com.ruoyi.erp.service.IErpBatchInfoService;
import com.ruoyi.common.core.text.Convert;

/**
 * 生产批次Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpBatchInfoServiceImpl implements IErpBatchInfoService 
{
    @Autowired
    private ErpBatchInfoMapper erpBatchInfoMapper;

    /**
     * 查询生产批次
     * 
     * @param id 生产批次ID
     * @return 生产批次
     */
    @Override
    public ErpBatchInfo selectErpBatchInfoById(String id)
    {
        return erpBatchInfoMapper.selectErpBatchInfoById(id);
    }

    /**
     * 查询生产批次列表
     * 
     * @param erpBatchInfo 生产批次
     * @return 生产批次
     */
    @Override
    public List<ErpBatchInfo> selectErpBatchInfoList(ErpBatchInfo erpBatchInfo)
    {
        return erpBatchInfoMapper.selectErpBatchInfoList(erpBatchInfo);
    }

    /**
     * 新增生产批次
     *
     * @param erpBatchInfo 生产批次
     * @return 结果
     */
    @Override
    public int insertErpBatchInfo(ErpBatchInfo erpBatchInfo) {
        erpBatchInfo.setId(IdUtils.fastSimpleUUID());
        erpBatchInfo.setCreateTime(DateUtils.getNowDate());

        // 查询数据库今天是第几条批次
        ErpBatchInfo query = new ErpBatchInfo();
        Map<String, Object> params = new HashMap<>();
        params.put("beginCreateTime", DateUtils.getDate());
        params.put("endCreateTime", DateUtils.getAddNumDate(24));
        query.setParams(params);
        List<ErpBatchInfo> list = erpBatchInfoMapper.selectErpBatchInfoList(query);
        int num = 10000 + list.size() + 1;
        erpBatchInfo.setBatchNo("P" + DateUtils.dateTime() + num);
        return erpBatchInfoMapper.insertErpBatchInfo(erpBatchInfo);
    }

    /**
     * 修改生产批次
     * 
     * @param erpBatchInfo 生产批次
     * @return 结果
     */
    @Override
    public int updateErpBatchInfo(ErpBatchInfo erpBatchInfo)
    {
        erpBatchInfo.setUpdateTime(DateUtils.getNowDate());
        return erpBatchInfoMapper.updateErpBatchInfo(erpBatchInfo);
    }

    /**
     * 删除生产批次对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpBatchInfoByIds(String ids)
    {
        return erpBatchInfoMapper.deleteErpBatchInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除生产批次信息
     * 
     * @param id 生产批次ID
     * @return 结果
     */
    @Override
    public int deleteErpBatchInfoById(String id)
    {
        return erpBatchInfoMapper.deleteErpBatchInfoById(id);
    }
}
