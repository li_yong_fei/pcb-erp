package com.ruoyi.erp.service;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.erp.domain.ErpCustomer;

/**
 * 客户Service接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface IErpCustomerService 
{
    /**
     * 查询客户
     * 
     * @param id 客户ID
     * @return 客户
     */
    public ErpCustomer selectErpCustomerById(String id);

    /**
     * 查询客户列表
     * 
     * @param erpCustomer 客户
     * @return 客户集合
     */
    public List<ErpCustomer> selectErpCustomerList(ErpCustomer erpCustomer);

    /**
     * 新增客户
     * 
     * @param erpCustomer 客户
     * @return 结果
     */
    public int insertErpCustomer(ErpCustomer erpCustomer);

    /**
     * 修改客户
     * 
     * @param erpCustomer 客户
     * @return 结果
     */
    public int updateErpCustomer(ErpCustomer erpCustomer);

    /**
     * 批量删除客户
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpCustomerByIds(String ids);

    /**
     * 删除客户信息
     * 
     * @param id 客户ID
     * @return 结果
     */
    public int deleteErpCustomerById(String id);

    /**
     * 导入用户数据
     *
     * @param list 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importData(List<ErpCustomer> list, Boolean isUpdateSupport, String operName);
}
