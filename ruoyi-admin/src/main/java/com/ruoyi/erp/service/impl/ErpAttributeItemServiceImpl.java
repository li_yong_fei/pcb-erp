package com.ruoyi.erp.service.impl;

import java.util.List;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpAttributeItemMapper;
import com.ruoyi.erp.domain.ErpAttributeItem;
import com.ruoyi.erp.service.IErpAttributeItemService;
import com.ruoyi.common.core.text.Convert;

/**
 * 拓展属性值Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpAttributeItemServiceImpl implements IErpAttributeItemService 
{
    @Autowired
    private ErpAttributeItemMapper erpAttributeItemMapper;

    /**
     * 查询拓展属性值
     * 
     * @param id 拓展属性值ID
     * @return 拓展属性值
     */
    @Override
    public ErpAttributeItem selectErpAttributeItemById(String id)
    {
        return erpAttributeItemMapper.selectErpAttributeItemById(id);
    }

    /**
     * 查询拓展属性值列表
     * 
     * @param erpAttributeItem 拓展属性值
     * @return 拓展属性值
     */
    @Override
    public List<ErpAttributeItem> selectErpAttributeItemList(ErpAttributeItem erpAttributeItem)
    {
        return erpAttributeItemMapper.selectErpAttributeItemList(erpAttributeItem);
    }

    /**
     * 新增拓展属性值
     * 
     * @param erpAttributeItem 拓展属性值
     * @return 结果
     */
    @Override
    public int insertErpAttributeItem(ErpAttributeItem erpAttributeItem)
    {
        erpAttributeItem.setId(IdUtils.fastSimpleUUID());
        erpAttributeItem.setCreateTime(DateUtils.getNowDate());
        return erpAttributeItemMapper.insertErpAttributeItem(erpAttributeItem);
    }

    /**
     * 修改拓展属性值
     * 
     * @param erpAttributeItem 拓展属性值
     * @return 结果
     */
    @Override
    public int updateErpAttributeItem(ErpAttributeItem erpAttributeItem)
    {
        erpAttributeItem.setUpdateTime(DateUtils.getNowDate());
        return erpAttributeItemMapper.updateErpAttributeItem(erpAttributeItem);
    }

    /**
     * 删除拓展属性值对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpAttributeItemByIds(String ids)
    {
        return erpAttributeItemMapper.deleteErpAttributeItemByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除拓展属性值信息
     * 
     * @param id 拓展属性值ID
     * @return 结果
     */
    @Override
    public int deleteErpAttributeItemById(String id)
    {
        return erpAttributeItemMapper.deleteErpAttributeItemById(id);
    }
}
